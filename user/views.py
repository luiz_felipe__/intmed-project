from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.template import RequestContext

from user.models import UserProfile
from .forms import SignUpForm, SignInForm
from django.contrib.auth.models import Group


def sign_up(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.is_staff = True
            group = Group.objects.get(name='client')
            user.groups.add(group)
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect("/admin/")
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def sign_in(request):
    if request.method == 'POST':
        data = dict(request.POST)
        user = UserProfile.objects.get(email=data['email'][0], username=data['username'][0])
        login(request, user)
        return redirect("/admin/")
    else:
        form = SignInForm()
        return render(request, 'login.html', {'form': form})
