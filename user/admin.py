from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from user.models import UserProfile

# Register your models here.


class UserProfileAdmin(UserAdmin):
    search_fields = ['name']

    def get_queryset(self, request):
        if request.user.is_superuser:
            qs = super(UserProfileAdmin, self).get_queryset(request)
            return qs.all()
        else:
            qs = super(UserProfileAdmin, self).get_queryset(request)
            return qs.filter(id=request.user.id)

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            if not obj:
                # the changelist itself
                return True
            return obj
        else:
            if not obj:
                # the changelist itself
                return True
            return obj.id == request.user.id


admin.site.register(UserProfile, UserProfileAdmin)
