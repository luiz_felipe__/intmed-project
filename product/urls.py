from rest_framework.routers import DefaultRouter
from product import views as product_views

product_router = DefaultRouter()
product_router.register(r'products', product_views.ProductViewSet)
