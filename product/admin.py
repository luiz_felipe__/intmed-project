from django.contrib import admin
from product.models import Product, Stock

# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    search_fields = ['name']

    def save_model(self, request, obj, form, change):
        super(ProductAdmin, self).save_model(request, obj, form, change)
        items_sold = 0
        remaining_quantity = Product.objects.get(id=obj.id).amount - items_sold
        data = {
            "product": obj,
            "items_sold": items_sold,
            "remaining_quantity": remaining_quantity,
        }
        Stock.objects.create(**data)


class StockAdmin(admin.ModelAdmin):
    search_fields = ['product_name']


admin.site.register(Product, ProductAdmin)
admin.site.register(Stock, StockAdmin)
