from django.db import models
from category.models import Category

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=60)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    amount = models.IntegerField()

    def __str__(self):
        return 'categoria: {} - produto: {} - id: {}'.format(self.category.name, self.name, self.id)


class Stock(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    items_sold = models.IntegerField(null=True, blank=True)
    remaining_quantity = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return 'id: {} - product: {} - quantidade de itens: {} - itens vendidos: {} - itens restantes: {}'.format(
            self.product.id, self.product.name, self.product.amount, self.items_sold, self.remaining_quantity
        )
