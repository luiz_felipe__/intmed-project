from django.db import models
# Create your models here.


class Trello(models.Model):
    key = models.CharField(max_length=100)
    token = models.CharField(max_length=200)
    active = models.BooleanField(default=False)

    def __str__(self):
        return '{} - token: {} - status: {}'.format(self.key, self.token, self.active)