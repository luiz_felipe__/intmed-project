from django.db import models
from product.models import Product
from user.models import UserProfile
# Create your models here.


class OrderStatus(models.Model):
    id_list_trello = models.CharField(max_length=50, null=True, blank=True,)
    status = models.CharField(max_length=30, null=True, default="")

    def __str__(self):
        return '{} - id: {}'.format(self.status, self.id)


class Order(models.Model):
    client = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='order_client')
    product = models.ManyToManyField(Product)
    order_status = models.ForeignKey(OrderStatus, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return '{} - id: {}'.format(self.client.username, self.id)


class JsonTrello(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    boards = models.TextField(null=True)
    lists = models.TextField(null=True)
    cards = models.TextField(null=True)
