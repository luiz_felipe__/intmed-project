import threading
import time
import requests
import json
from django.contrib import admin
from category.models import Category
from order.models import Order, JsonTrello, OrderStatus
from django import forms
from intmed import settings
from product.models import Product, Stock
from trello.models import Trello
from user.models import UserProfile
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
# Register your models here.


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['product'].help_text = 'Selecione pelo menos um produto de cada categoria'

    def clean(self):
        categories = []
        products = self.cleaned_data["product"]
        categories_queryset = Category.objects.all()
        trello = Trello.objects.filter(active=True).exists()
        if trello:
            if len(products) >= len(categories_queryset):
                for item in products:
                    categories.append(item.category.name)
            else:
                raise forms.ValidationError({'product': 'Selecione pelo menos um produto de cada categoria.'})

            for item in categories_queryset:
                if not item.name in categories:
                    raise forms.ValidationError({'product': 'Selecione pelo menos um produto de cada categoria.'})
        else:
            raise forms.ValidationError({'product': 'Por favor selecione, a chave e token do trello para cotinuar a operação.'})
        return super(OrderForm, self).clean()


class OrderAdmin(admin.ModelAdmin):
    form = OrderForm
    filter_horizontal = ('product',)
    search_fields = ['product_name']
    readonly_fields = ('order_status', )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'client':
            kwargs['initial'] = request.user.id
        return super(OrderAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def get_form(self, request, obj=None, **kwargs):
        form = super(OrderAdmin, self).get_form(request, obj=None, **kwargs)
        form.base_fields['client'].queryset = UserProfile.objects.filter(id=request.user.id)
        return form

    def set_board(self, trello):
        # criação de quadro
        url_boards = "https://api.trello.com/1/boards/"
        querystring_boards = {"name": "Pedidos", "defaultLabels": "true", "defaultLists": "true",
                              "keepFromSource": "none",
                              "prefs_permissionLevel": "private", "prefs_voting": "disabled",
                              "prefs_comments": "members",
                              "prefs_invitations": "members", "prefs_selfJoin": "true", "prefs_cardCovers": "true",
                              "prefs_background": "blue", "prefs_cardAging": "regular",
                              "key": trello.key,
                              "token": trello.token}
        response_boards = requests.request("POST", url_boards, params=querystring_boards)
        return response_boards

    def search_lists_board(self, board, trello):
        # busca de listas do quadro
        url_boards_list = "https://api.trello.com/1/boards/" + board + "/lists"
        querystring_boards_list = {"key": trello.key, "token": trello.token}
        response_boards_list = requests.request("GET", url_boards_list, params=querystring_boards_list)
        for item in json.loads(response_boards_list.text):
            OrderStatus.objects.filter(status__icontains=item['name']).update(id_list_trello=item['id'])
        return response_boards_list

    def set_first_list(self, response_boards_list, all_statuses, trello):
        # definição da primeira lista
        list = json.loads(response_boards_list.text)
        url_list = "https://api.trello.com/1/lists/" + list[0]['id']
        initial_status = all_statuses.order_by('id')[0]
        querystring_list = {"name": initial_status.status, "pos": initial_status.id, "key": trello.key,
                            "token": trello.token}
        response_list = requests.request("PUT", url_list, params=querystring_list)
        return response_list

    def set_lists_board(self, all_statuses, board, trello):
        # definição das listas
        status_list = all_statuses.order_by('id')[1:4]
        for item in status_list:
            url = "https://api.trello.com/1/boards/" + board['id'] + "/lists"
            querystring = {"name": str(item.status), "pos": item.id, "key": trello.key, "token": trello.token}
            response = requests.request("POST", url, params=querystring)
        return response

    def set_card(self, msg_client, msg_parts, trello, list):
        url = "https://api.trello.com/1/cards"
        querystring = {"name": "Pedido de montagem",
                       "desc": msg_client + msg_parts,
                       "idList": list[0]['id'], "keepFromSource": "all",
                       "key": trello.key,
                       "token": trello.token}
        response_card = requests.request("POST", url, params=querystring)
        return response_card

    def set_insertion_trello(self, orders, trello, all_statuses, obj):
        if len(orders) == 1:
            response_boards = self.set_board(trello)
            board = json.loads(response_boards.text)
            response_boards_list = self.search_lists_board(board['id'], trello)
            response_first_list = self.set_first_list(response_boards_list, all_statuses, trello)
            response_lists = self.set_lists_board(all_statuses, board, trello)
            data_trello = {
                "boards": response_boards.text,
                "lists": response_boards_list.text,
                "order": obj
            }
            JsonTrello.objects.create(**data_trello)
        else:
            board = json.loads(JsonTrello.objects.last().boards)
        return board

    def set_stock(self, products):
        for item in products:
            product = Stock.objects.filter(product__id=item.id).exists()
            if product:
                items_sold = Stock.objects.get(product__id=item.id).items_sold + 1
                remaining_quantity = Product.objects.get(id=item.id).amount - items_sold
                Stock.objects.filter(product__id=item.id).update(items_sold=items_sold, remaining_quantity=remaining_quantity)

    def get_queryset(self, request):
        if request.user.is_superuser:
            qs = super(OrderAdmin, self).get_queryset(request)
            return qs.all()
        else:
            qs = super(OrderAdmin, self).get_queryset(request)
            return qs.filter(client__id=request.user.id)

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            if not obj:
                # the changelist itself
                return True
            return obj
        else:
            if not obj:
                # the changelist itself
                return True
            return obj.client.id == request.user.id

    def save_model(self, request, obj, form, change):
        super(OrderAdmin, self).save_model(request, obj, form, change)
        client = obj.client
        orders = Order.objects.all()
        products = form.cleaned_data["product"]
        all_statuses = OrderStatus.objects.all()
        trello = Trello.objects.filter(active=True).last()
        board = self.set_insertion_trello(orders, trello, all_statuses, obj)

        json_boards = JsonTrello.objects.last()
        list = json.loads(json_boards.lists)
        msg_client = "cliente: " + str(client.name) + "\n" + "email: " + str(client.email) + "\n" + "telefone: " + str(client.phone) + "\n"
        msg_parts = "Componentes: \n"

        for item in products:
            msg_parts = msg_parts + str(item.name) + "\n"
        response_card = self.set_card(msg_client, msg_parts, trello, list)
        update_lists = self.search_lists_board(board['id'], trello)
        JsonTrello.objects.filter(id=json_boards.id).update(cards=response_card.text, lists=update_lists.text)
        self.set_stock(products)


class OrderStatusAdmin(admin.ModelAdmin):
    search_fields = ['status']
    readonly_fields = ('id_list_trello', )


class get_status_order(OrderAdmin):
    def __init__(self, interval=300):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        try:
            trello = Trello.objects.filter(active=True).last()
            while True:
                queryset = JsonTrello.objects.all()
                if queryset:
                    for item in queryset:
                        cards = json.loads(item.cards)
                        url = "https://api.trello.com/1/cards/" + str(cards['id'])
                        querystring = {"key": trello.key, "token": trello.token}
                        response = requests.request("GET", url, params=querystring)
                        card_response = json.loads(response.text)
                        if cards['idList'] == card_response['idList']:
                            print("Sem alterações de estado do cartão")
                        else:
                            print("Alteração identificada")
                            JsonTrello.objects.filter(order__id=item.order.id).update(cards=response.text)
                            new_status = OrderStatus.objects.get(id_list_trello=card_response['idList'])
                            Order.objects.filter(id=item.order.id).update(order_status=new_status)
                time.sleep(self.interval)
        except ValidationError as exc:
            return Response(exception=exc)


get_status_order()

admin.site.register(Order, OrderAdmin)
admin.site.register(JsonTrello)
admin.site.register(OrderStatus, OrderStatusAdmin)
