from rest_framework.routers import DefaultRouter
from order import views as order_views

order_router = DefaultRouter()
order_router.register(r'orders', order_views.OrderViewSet)
