# Generated by Django 2.1 on 2018-08-11 20:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0002_auto_20180811_2028'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(1, 'REALIZADO'), (2, 'SEPARAÇÃO EM ESTOQUE'), (2, 'EM MONTAGEM'), (3, 'REALIZAÇÃO DE TESTES'), (4, 'CONCLUÍDO')])),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('product', models.ManyToManyField(to='product.Product')),
            ],
        ),
    ]
