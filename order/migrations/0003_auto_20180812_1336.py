# Generated by Django 2.1 on 2018-08-12 13:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20180812_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(1, 'REALIZADO'), (2, 'SEPARAÇÃO EM ESTOQUE'), (2, 'EM MONTAGEM'), (3, 'REALIZAÇÃO DE TESTES'), (4, 'CONCLUÍDO')])),
            ],
        ),
        migrations.RemoveField(
            model_name='order',
            name='status',
        ),
        migrations.AlterField(
            model_name='order',
            name='product',
            field=models.ManyToManyField(to='product.Product'),
        ),
        migrations.AddField(
            model_name='orderstatus',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
    ]
