# Generated by Django 2.1 on 2018-08-14 22:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0010_auto_20180814_2129'),
    ]

    operations = [
        migrations.AddField(
            model_name='jsontrello',
            name='lists',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='jsontrello',
            name='boards',
            field=models.TextField(null=True),
        ),
    ]
