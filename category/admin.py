from django.contrib import admin
from category.models import Category
# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['name']


admin.site.register(Category, CategoryAdmin)
