from rest_framework.routers import DefaultRouter
from category import views as category_views

category_router = DefaultRouter()
category_router.register(r'categories', category_views.CategoryViewSet)
